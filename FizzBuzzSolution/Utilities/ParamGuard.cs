﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    /// <summary>
    /// 	Helps ensure that invalid arguments are not specified for methods
    /// 	of various classes.
    /// </summary>
    public static class ParamGuard
    {
        #region Public static methods.

        /// <summary>
        /// 	Ensures that the passed in argument is not null.
        /// </summary>
        /// <param name="argumentToVerify"> The argument to be verified that it doesn't carry a null value. </param>
        /// <param name="nameOfArgument"> The name of argument. </param>
        public static void VerifyArgument(
            object argumentToVerify,
            string nameOfArgument)
        {
            // Is the argument's value null?
            if (argumentToVerify == null)
            {
                // Yes. Throw an appropriate exception.
                throw (new ArgumentNullException(nameOfArgument));
            }
        }

        /// <summary>
        /// 	Ensures that the passed in argument is not null.
        /// </summary>
        /// <param name="argumentToVerify"> The string argument to be verified that it doesn't carry a null or empty string value. </param>
        /// <param name="nameOfArgument"> The name of argument. </param>
        public static void VerifyArgument(
             string argumentToVerify,
            string nameOfArgument)
        {
            // Is the argument's value null?
            if (String.IsNullOrEmpty(argumentToVerify) ||
                String.IsNullOrWhiteSpace(argumentToVerify))
            {
                // Yes. Throw an appropriate exception.
                throw (new ArgumentNullException(nameOfArgument));
            }
        }

        #endregion
    }
}
