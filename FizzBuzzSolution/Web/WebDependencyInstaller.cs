﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System.Web.Mvc;
using Utilities;

namespace Web
{
    public class WebDependencyInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Installs required components to given container.
        /// </summary>
        /// <param name="container"Input container.></param>
        /// <param name="store">Configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            ParamGuard.VerifyArgument(container, "IWindsorContainer cannot be empty or null");
            ParamGuard.VerifyArgument(store, "IConfigurationStore cannot be empty or null");

            container.Register(
                Classes.FromAssemblyNamed("Web").BasedOn<IController>().LifestyleTransient()
                );
        }
    }
}

