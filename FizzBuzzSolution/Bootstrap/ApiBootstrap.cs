﻿using Castle.Windsor;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace Bootstrap
{
    public static class ApiBootstrap
    {        
        public static void Start(IWindsorContainer container)
        {
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new ApiWindsorControllerFactory(container));
        }
    }
}
