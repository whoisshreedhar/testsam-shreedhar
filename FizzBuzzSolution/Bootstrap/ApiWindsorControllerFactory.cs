﻿using Castle.Windsor;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace Bootstrap
{
    /// <summary>
    /// Api controller factory.
    /// </summary>
    class ApiWindsorControllerFactory : IHttpControllerActivator
    {
        /// <summary>
        /// Container.
        /// </summary>
        private readonly IWindsorContainer container;

        public ApiWindsorControllerFactory(IWindsorContainer container)
        {
            this.container = container;
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            if (controllerType == null) {
                throw new Exception("No controller found.");
            }

            var controller = (IHttpController)this.container.Resolve(controllerType);
            request.RegisterForDispose(new Release(
                () => this.container.Release(controller)
                ));

            return controller;
        }
    }
}
