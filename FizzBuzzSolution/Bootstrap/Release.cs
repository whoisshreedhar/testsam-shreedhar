﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bootstrap
{
    public class Release : IDisposable
    {
        public Action action;
        public Release(Action action)
        {
            this.action = action;
        }

        public void Dispose()
        {
            this.action();
        }
    }
}
