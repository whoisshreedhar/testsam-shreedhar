﻿using Castle.Windsor;
using System.Web.Mvc;

namespace Bootstrap
{
    public static class WebBootstrap
    {
        public static void Start(IWindsorContainer container)
        {
            ControllerBuilder.Current.SetControllerFactory(new WebWindsorControllerFactory(container.Kernel));
        }
    }
}
